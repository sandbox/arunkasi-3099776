A module to abbreavate numbers.

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Recommended modules
 * Requirements
 * Usage
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers


INTRODUCTION
------------

Number Abbrevation module provide field formatter and a twig filter to shorten
large numbers into abbreviations.

For example:

I have a number, e.g. 1000, 1230, 1500, 154000, 1500000, 1000000

And I want to format them with an abbreviation. i.e.

if it's a thousand, then 1k, 1.1k, 2k, 10k, 10.5k etc

RECOMMENDED MODULES
-------------------

* No extra module is required.

REQUIREMENTS
------------

* No requirements needed.

USAGE
-----
Enable the module you will get a field formatter 'Abbrevate Number' for 
`integer`, `decimal` and `float` field types.
You can also use a twig filter `abbrevate_number` for these fields field value.

### Service usage for other modules

`Drupal::service('number_abbrevation.abbrnumber')->abbrevateNumber($number)`

### Twig filter usage |abbrevate_number

`{{ field__value|abbrevate_number }}`

Thanks to Ben Cole for the original code:
https://gist.github.com/bcole808/9371754

INSTALLATION
------------

 * Install as usual, see
   https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8 for further
   information.

CONFIGURATION
-------------

 * No configuration is needed.

TROUBLESHOOTING
---------------

 * This module doesn't provide any visible functions to the user on its own, it
   just provides field formatter and a twig filter.

MAINTAINERS
-----------

Current maintainers:

 * Arun Kasinathan (https://www.drupal.org/user/3612276)
