<?php

namespace Drupal\number_abbrevation;

/**
 * Provides an interface defining an Example entity.
 */
interface NumberAbbrevationInterface {

  /**
   * Shorten large numbers into abbreviations (i.e. 1,500 = 1.5k).
   *
   * @param int $number
   *   The value to be abbrevated.
   *
   * @return string
   *   Returns abbrevated numbers.
   */
  public function abbrevateNumber($number);

}
