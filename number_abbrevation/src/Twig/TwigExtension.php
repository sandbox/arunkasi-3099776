<?php

namespace Drupal\number_abbrevation\Twig;

use Drupal\number_abbrevation\NumberAbbrevation;

/**
 * Defines a controller for TwigExtension.
 */
class TwigExtension extends \Twig_Extension {

  /**
   * Constructs a new TwigExtension object.
   */
  public function __construct(NumberAbbrevation $number_abbrevation) {
    $this->numberAbbrevation = $number_abbrevation;
  }

  /**
   * {@inheritdoc}
   */
  public function getFilters() {

    // |abbrevate_number.
    $filters[] = new \Twig_SimpleFilter('abbrevate_number', [$this, 'abbrevateNumber']);

    return $filters;
  }

  /**
   * Abbrevates given number,float,decimal.
   *
   * @param int|float $number
   *   The count value to be converted.
   *
   * @return string
   *   The abbrevated value to be returned.
   */
  public function abbrevateNumber($number) {

    return $this->numberAbbrevation->abbrevateNumber($number);

  }

}
