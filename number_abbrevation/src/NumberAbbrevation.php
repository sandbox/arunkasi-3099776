<?php

namespace Drupal\number_abbrevation;

/**
 * Class NumberAbbrevation.
 */
class NumberAbbrevation implements NumberAbbrevationInterface {

  /**
   * Shorten large numbers into abbreviations (i.e. 1,500 = 1.5k).
   *
   * @param int|float $number
   *   The value to be abbrevated.
   *
   * @return string
   *   Returns abbrevated number format.
   */
  public function abbrevateNumber($number) {

    $number = ($number == (int) $number) ? (int) $number : (float) $number;

    $abbrevs = [
      24 => "SP",
      21 => "SX",
      18 => "QT",
      15 => "QD",
      12 => "T",
      9 => "B",
      6 => "M",
      3 => "K",
      0 => "",
    ];

    foreach ($abbrevs as $exponent => $abbrev) {
      if ($number >= pow(10, $exponent)) {
        $display_num = $number / pow(10, $exponent);
        $decimals = ($exponent >= 3 && round($display_num) < 100) ? 1 : 0;
        return number_format($display_num, $decimals) . $abbrev;
      }

    }
    return $number;
  }

}
