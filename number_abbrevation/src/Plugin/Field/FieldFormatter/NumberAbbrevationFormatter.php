<?php

namespace Drupal\number_abbrevation\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\number_abbrevation\NumberAbbrevation;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'number_abbrevation' formatter.
 *
 * @FieldFormatter(
 *   id = "number_abbrevation",
 *   module = "number_abbrevation",
 *   label = @Translation("Abbrevate Number"),
 *   field_types = {
 *     "decimal", "float", "integer"
 *   }
 * )
 */
class NumberAbbrevationFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs a StringFormatter instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\number_abbrevation\NumberAbbrevation $number_abbrevation
   *   The number abbrevation service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, NumberAbbrevation $number_abbrevation) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->numberAbbrevation = $number_abbrevation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('number_abbrevation.abbrnumber')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Displays Abbrevated Number. Shorten large numbers into abbreviations (i.e. 1,500 = 1.5k).');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      if ($item->value) {
        $dataValue = $item->value;
      }
      else {
        $dataValue = 0;
      }
      // Render each element as markup.
      $element[$delta] = ['#markup' => $this->numberAbbrevation->abbrevateNumber($dataValue)];
    }

    return $element;
  }

}
